﻿using System;
using Kalynenko.Viktor.RobotChallenge;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace KalynenkoViktor.RobotChallenge.Test
{
    [TestClass]
    public class RobotAlgorithm
    {
        [TestMethod]
        public void TestMethod1()
        {
            var algorithm = new KalynenkoViktorAlgorithm();
            var map = new Robot.Common.Map();
            var StationPosition = new Robot.Common.Position(1, 1);
            map.Stations.Add(new Robot.Common.EnergyStation() { Energy=1000, Position=StationPosition, RecoveryRate=2 });

            var robots = new System.Collections.Generic.List<Robot.Common.Robot>
            { new Robot.Common.Robot() { Energy=200, Position= new Robot.Common.Position(2,3)} };

            var command = algorithm.DoStep(robots,0,map);
            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, StationPosition);
        }
    }
}
