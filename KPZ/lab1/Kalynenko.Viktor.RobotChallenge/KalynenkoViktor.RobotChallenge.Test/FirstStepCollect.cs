﻿using System;
using System.Collections.Generic;
using Kalynenko.Viktor.RobotChallenge;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;




namespace KalynenkoViktor.RobotChallenge.Test
{
    [TestClass]
    public class FirstStepCollect
    {
        [TestMethod]
        public void TestMethod1()
        {

            var robot1 = new Robot.Common.Robot() { Energy = 100, Position = new Robot.Common.Position(1, 1) };
            
            var algh = new KalynenkoViktorAlgorithm();
            var station1Pos = new Position(1, 1);
            
            List<EnergyStation> stations = new List<EnergyStation>() { };
            stations.Add(new EnergyStation() { Position = station1Pos });
            
            var map = new Map()
            {
                Stations = stations
            };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            {
                robot1,
            };
            var command = algh.DoStep(robots, 0, map);

            Assert.IsTrue(command is CollectEnergyCommand);


            //Assert.IsTrue(KalynenkoViktorAlgorithm.IsCellFree(station1Pos, robot2, robots));

        }
    }
}
