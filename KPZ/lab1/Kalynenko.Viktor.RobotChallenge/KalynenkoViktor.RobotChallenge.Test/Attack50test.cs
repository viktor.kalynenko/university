﻿using System;
using System.Collections.Generic;
using Kalynenko.Viktor.RobotChallenge;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace KalynenkoViktor.RobotChallenge.Test
{
    [TestClass]
    public class Attack50test
    {
        [TestMethod]
        public void TestMethod1()
        {

            var robot1 = new Robot.Common.Robot() { Energy = 100, Position = new Robot.Common.Position(6, 6)};
            
            var robot2 = new Robot.Common.Robot() { Energy = 100, Position = new Robot.Common.Position(4, 4) };
            
            var algh = new KalynenkoViktorAlgorithm();
            var station1Pos = new Position(6, 6);
            var station2Pos = new Position(4, 4);
            List<EnergyStation> stations = new List<EnergyStation>() { };
            stations.Add(new EnergyStation() { Position = station1Pos });
            stations.Add(new EnergyStation() { Position = station2Pos });
            var map = new Map()
            {
                Stations = stations
            };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            {
                robot1,robot2,
            };

            int i = 0;
            algh.RoundCounter = 51;
            MoveCommand moveCommand = (MoveCommand)algh.DoStep(robots, 1, map);

            Assert.AreEqual(moveCommand.NewPosition, station1Pos);


        }
    }
}
