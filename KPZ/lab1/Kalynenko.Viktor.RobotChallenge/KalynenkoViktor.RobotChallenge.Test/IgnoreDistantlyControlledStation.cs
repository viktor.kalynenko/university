﻿using System;
using System.Collections.Generic;
using Kalynenko.Viktor.RobotChallenge;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace KalynenkoViktor.RobotChallenge.Test
{
    [TestClass]
    public class IgnoreDistantlyControlledStation
    {
        [TestMethod]
        public void TestMethod1()
        {
            var robot1 = new Robot.Common.Robot() { Energy = 100, Position = new Robot.Common.Position(9, 10) };
            var robot2 = new Robot.Common.Robot() { Energy = 100, Position = new Robot.Common.Position(5, 6) };
            var algh = new KalynenkoViktorAlgorithm();
            var station1Pos = new Position(9, 10);
            var station2Pos = new Position(1, 1);
            List<EnergyStation> stations = new List<EnergyStation>() { };
            stations.Add(new EnergyStation() { Position = station1Pos });
            stations.Add(new EnergyStation() { Position = station2Pos });
            var map = new Map()
            {
                Stations = stations
            };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            {
                robot1,robot2
            };

            Position aim = new Position(1, 1);

            MoveCommand moveCommand = (MoveCommand)algh.DoStep(robots, 1, map);
            Assert.AreEqual(aim, moveCommand.NewPosition);
            
        }
    }
}
