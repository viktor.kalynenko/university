﻿using System;
using Kalynenko.Viktor.RobotChallenge;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace KalynenkoViktor.RobotChallenge.Test
{
    [TestClass]
    public class TestDistanceHelper
    {
        [TestMethod]
        public void TestDistance()
        {
            var p1 = new Position(1, 1);
            var p2 = new Position(2, 4);
           int a= DistanceHelper.FindDistance(p1, p2);
            Assert.AreEqual(10, a);

        }
    }
}
